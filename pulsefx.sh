#!/bin/bash

# Nom PulseFx V 1.0
# Date: 16/04/2017
# Dev: Shell
# Auteurs: KURO

#### Configuration ################################################################

#### Colors #######################################################################

blanc="\033[1;37m"
gris="\033[0;37m"
rouge="\033[1;31m"
vert="\033[1;32m"
jaune="\033[1;33m"

nar=" 	M E N U "
Titre=" PulseFx v1.0 "

###### CONFIG FIN #################################################################


#### INTRO ########################################################################

function PRE {

	clear
	echo
	sleep 0.5
	echo -e " 
	 ______         ___                     
	/\  __ \       /\_ \                    
	\ \ \_\ \__  __\//\ \     ____    ____   
	 \ \  __/\ \/\ \ \ \ \   / ,__\  / ___\ 
	  \ \ \/\ \ \_\ \ \_\ \_/\__,  \/\  __\_ 
	   \ \_\ \ \____/ /\____\/\____/\ \_____\
	    
	    \/_/  \/___/  \/____/\/___/  \/_____/ 
"	
	sleep 0.5
	echo -e "$vert		 ______   __  __    
		/\  ___\ /\_\_\_\   
		\ \  __\ \/_/\_\/_  
		 \ \_\_/   /\_\/\_\ 
		  \/_/     \/_/\/_/ "
	echo
	echo -e "    		$rouge P$blanc""ulseFX Kali 2.0$vert By$jaune KU$rouge""RO $blanc"
	sleep 5
	main

}

############ INTRO FIN #############################################################



######## MENU 1 ####################################################################

function main {

	clear

	echo

	top

	echo
	echo
	echo -e "  $blanc$nar"
	echo -e " 
	$vert 1)$blanc Info
	$vert 2)$blanc Fix  
	$rouge 0)$blanc Fermer le programme "
	echo
	read -p " Choix: " choix
		case $choix in
			1) information;;
			2) FIX;;
			0) EXITMENU;;
			*) echo -e "$Blanc [$rouge ERREUR$blanc ]" && sleep 2
		esac
	
main
}

####### MENU 1 FIN ##################################################################


### INFO ############################################################################

function information {

	clear

	echo

	top

	echo	
	echo -e "$vert	Nom:$blanc.......PulseFx"
	sleep 0.2
	echo -e "$vert	Version:$blanc...1.0"
	sleep 0.2
	echo -e "$vert	Langue:$blanc....[FR]"
	sleep 0.2
	echo -e "$vert	Date:$blanc......16/04/2017"
	sleep 0.2
	echo -e "$vert	Auteurs:$blanc...KURO"
	sleep 0.2
	echo -e "$vert	Type:$blanc......Fix audio bug"
	sleep 0.2
	echo -e "$vert	Dev:$blanc.......Shell "
	echo
	echo
	echo -e " Regle le probleme audio (kali 2.0)"
	echo
	sleep 0.5 
	echo -e " Faire$rouge [ENTREE]"
	read pause		 
main
}

###### INFO FIN ####################################################################


######### FIX ###########################################################

function FIX {
	
	clear

	echo

	top

	echo
	echo
	echo -e " $blanc E T A T"
	sleep 1
	cp daemon.conf /etc/pulse/
	sleep 1
	cp .bashrc ~/
	sleep 1
	pulseaudio --start
	sleep 1
	echo -e "
	$blanc[$vert*$blanc]$vert F I X E D $blanc[$vert*$blanc]"
	sleep 3

EXITMENU

}

######## FIX FIN ######################################################


##### Fonction Top ###############################################################

function top {

	clear
	echo
	sleep 0.1 
	echo -e " $blanc~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" 
	echo -e " $rouge 	P$blanc U L S E  $rouge""F$blanc"" X $gris  v$vert 1.0 " 
	echo -e " $blanc~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	sleep 0.5
	echo

}

#### FONTION TOP FIN ################################################################


##### FERMETURE DU PROGRAMME ######################################################

function EXITMENU {

	clear
	echo
	top
	echo -e "$blanc Fermeture du programme..."
	sleep 2
	echo
	sleep 0.5
	echo
	echo -e " [$rouge*$blanc] $vert Merci d'avoir utiliser PulseFx$blanc [$rouge*$blanc]"
	sleep 3
	clear
	exit
}

###### FIN (END) #################################################################

PRE

